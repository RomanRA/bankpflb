package ru.pflb.bank.functional;

import lib.NameBankCompanyForHolder;

import static lib.Holder.*;
import static lib.NameBankCompanyForHolder.BANK;
//import static lib.BANK;

/**
 * Организует обработку номера счета и компании
 */
public class PersonalAccount {

    public String createInvoice(NameBankCompanyForHolder nameCompany, String invoiceCompany, double money) {
        setPersAccounts(nameCompany, invoiceCompany, money);
        return "Компания - \"" + holderCompany.get(nameCompany).name() +
                "\" \nдобавлен счет № " + holderCompany.get(nameCompany).persAccounts() +
                "\nс суммой " + money + " руб.";
    }

    private void setPersAccounts(NameBankCompanyForHolder nameCompany, String persAcc, double money) {
        createCompanylAccount(nameCompany);

        if (holderBank.get(BANK).persAccounts().remove(persAcc)) {
            holderBank.get(BANK).persAccounts().add(persAcc);
            System.out.println("Этот номер счета уже существует, заведите новыйй счет\n" +
                    " Компания со счетом-дублером: \"" + holderCompany.get(nameCompany).name() +
                    "\"\n Существующий номер счета: " + persAcc
            );
        } else {
            holderBank.get(BANK).persAccounts().add(persAcc);
            holderCompany.get(nameCompany).persAccounts().add(persAcc);
            holderCompany.get(nameCompany).setSummInvoice(nameCompany,persAcc, money);
            holderBank.get(BANK).nameCompanyInvoice(holderCompany.get(nameCompany).name(), holderCompany.get(nameCompany).persAccounts());
        }
    }

    //    Можно было сделать и генератор, но время мало(((
    private void createCompanylAccount(NameBankCompanyForHolder nameCompany) {
        if (holderBank.get(BANK).persAccounts().size() == 0) {
            holderBank.get(BANK).persAccounts().add("12345.810.9.12345000000");
            holderBank.get(BANK).setSummInvoice(nameCompany,"12345.810.9.12345000000", 10000);
        }
        if (holderCompany.get(nameCompany).persAccounts().size() == 0) {
            switch (nameCompany) {
                case COMPANY_ROTE_ON_MARS:
                    holderCompany.get(nameCompany).persAccounts().add("12345.810.9.12345611111");
                    holderBank.get(BANK).persAccounts().add("12345.810.9.12345611111");
                    holderCompany.get(nameCompany).setSummInvoice(nameCompany,"12345.810.9.12345611111", 1000);
                    break;
                case MARIN_OCEAN_DIVING:
                    holderCompany.get(nameCompany).persAccounts().add("12345.810.9.12345622222");
                    holderBank.get(BANK).persAccounts().add("12345.810.9.12345622222");
                    holderCompany.get(nameCompany).setSummInvoice(nameCompany,"12345.810.9.12345622222", 100);
                    break;
                case SHOVEL_AND_FRIENDS:
                    holderCompany.get(nameCompany).persAccounts().add("12345.810.9.12345633333");
                    holderBank.get(BANK).persAccounts().add("12345.810.9.12345633333");
                    holderCompany.get(nameCompany).setSummInvoice(nameCompany,"12345.810.9.12345633333", 10);
                    break;
            }
        }
    }
}
