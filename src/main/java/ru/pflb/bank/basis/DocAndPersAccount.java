package ru.pflb.bank.basis;

import java.util.List;

public interface DocAndPersAccount {
    List<String> persAccounts();

}
