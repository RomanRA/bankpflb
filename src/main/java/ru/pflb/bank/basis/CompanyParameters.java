package ru.pflb.bank.basis;

public interface CompanyParameters {
    String name();
    String adress();
}
