package ru.pflb.bank.basis;

import lib.NameBankCompanyForHolder;

public interface OperationsWithMoney {
    void cutDownMoney(NameBankCompanyForHolder nameBankCompanyForHolder, String numberInvoiice, double cutDownSumma);

    void addMoney(NameBankCompanyForHolder nameBankCompanyForHolder, String numberInvoiice, double addSumma);
}
