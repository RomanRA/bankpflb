package ru.pflb.bank.basis;

import lib.NameBankCompanyForHolder;

import java.util.*;

import static lib.Holder.holderCompany;
import static lib.NameBankCompanyForHolder.COMPANY_ROTE_ON_MARS;

/**
 * Организация имеет название и адрес, содержит в себе списки со ссылками на свои лицевые счета и свои документы.
 * COMPANY_ROTE_ON_MARS - Марсианские перевозки
 * MARIN_OCEAN_DIVING - Морско-Океанский ныряльщик
 * SHOVEL_AND_FRIENDS- Лопата и ребята
 */
public class Companies implements CompanyParameters, DocAndPersAccount, OperationsWithMoney {
    private String name;
    private String adress;
    private ArrayList<String> persCompanyAccounts = new ArrayList<String>();
    private Map<NameBankCompanyForHolder, Double> companySummInvoice =
            new HashMap<NameBankCompanyForHolder, Double>();
    private Map<String, Double> summInvoice = new HashMap<String, Double>();

    public Companies() {
    }

    public Companies(String name, String adress) {
        this.name = name;
        this.adress = adress;
    }

    public Map<String, Double> getSummInvoice() {
        return summInvoice;
    }

    public void setSummInvoice(NameBankCompanyForHolder nameBankCompanyForHolder, String numberInvoice, double money) {
        this.setCompanySummInvoice(nameBankCompanyForHolder, money);
        this.summInvoice.put(numberInvoice, money);
    }

    public String name() {
        return name;
    }

    public String adress() {
        return adress;
    }

    public List<String> persAccounts() {
        return persCompanyAccounts;
    }

    public int moneyOnInvoice(String invoiceScore) {
        return persAccounts().indexOf(invoiceScore);
    }

    private void setCompanySummInvoice(NameBankCompanyForHolder nameBankCompanyForHolder, double companySummInvoice) {
        this.companySummInvoice.put(nameBankCompanyForHolder, companySummInvoice);
    }

    public Map<NameBankCompanyForHolder, Double> getCompanySummInvoice() {
        return companySummInvoice;
    }

    public void addMoney(NameBankCompanyForHolder nameBankCompanyForHolder, String numberInvoiice, double addSumma) {
        double newNumMoney = getSummInvoice().get(numberInvoiice) + addSumma;
        this.setSummInvoice(nameBankCompanyForHolder, numberInvoiice, newNumMoney);
        if (!this.companySummInvoice.containsKey(nameBankCompanyForHolder)) {
            this.companySummInvoice.put(nameBankCompanyForHolder, addSumma);
        } else {
            double nValue = this.companySummInvoice.replace(nameBankCompanyForHolder, getSummInvoice().get(numberInvoiice)) +
                    addSumma;
            this.companySummInvoice.replace(nameBankCompanyForHolder, nValue);
        }
        System.out.println("Сумма на счете " + numberInvoiice + ", после транзакции добавления на сумму:" + addSumma
                + "\n составляет = " + getSummInvoice().get(numberInvoiice));
        System.out.println("Сумма счета компании \"" + name() + "\" составляет = " + allMoneyOnPersonallAccounComany());
    }

    public void cutDownMoney(NameBankCompanyForHolder nameBankCompanyForHolder, String numberInvoiice, double cutDownSumma) {
        double newNumMoney = getSummInvoice().get(numberInvoiice) - cutDownSumma;
        if (newNumMoney <= 0) {
            System.out.println("Недостаточно средств на счете компании " + name() + "\n" +
                    "Баланс = " + getSummInvoice().get(numberInvoiice));
        } else {
            this.setSummInvoice(nameBankCompanyForHolder, numberInvoiice, newNumMoney);
            double nValue = this.companySummInvoice.replace(nameBankCompanyForHolder, getSummInvoice().get(numberInvoiice)) -
                    cutDownSumma;
            this.companySummInvoice.put(nameBankCompanyForHolder, nValue);
            System.out.println("Сумма на счете " + numberInvoiice + ", после транзакции списания на сумму:" + cutDownSumma
                    + "\n составляет = " + getSummInvoice().get(numberInvoiice));
            System.out.println("Сумма счета компании \"" + name() + "\" составляет = " + allMoneyOnPersonallAccounComany());
        }
    }

    public Double allMoneyOnPersonallAccounComany() {
        double summInvoices = 0;
        for (int i = 0; i < persAccounts().size(); i++) {
            if (getSummInvoice().get(persAccounts().get(i)) == 0) {
                summInvoices = getSummInvoice().get(persAccounts().get(i));
            } else {
                summInvoices += getSummInvoice().get(persAccounts().get(i));
            }
        }
        return summInvoices;
    }
}
