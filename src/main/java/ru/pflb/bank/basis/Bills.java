package ru.pflb.bank.basis;

import lib.NameBankCompanyForHolder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static lib.Holder.holderCompany;

public class Bills {
    private Map<Integer, String> billMemory = new HashMap<>();
    private int count = 1;

    public Map<Integer, String> getBillMemory() {
        return billMemory;
    }

    private void setBillMemory(int count, String billMemory) {
        this.billMemory.put(count, billMemory);
    }

    private int getCount() {
        return count;
    }

    private void setCount() {
        this.count++;
    }

    public String bill(NameBankCompanyForHolder payCompany, NameBankCompanyForHolder receiverCompany, double howMach) {
        String bill;
        double maxValuePay = Collections.max(holderCompany.get(payCompany).getSummInvoice().values()) - howMach;
        String maxKey = getKeyFromValue(holderCompany.get(payCompany).getSummInvoice(),
                Collections.max(holderCompany.get(payCompany).getSummInvoice().values()));

        if (maxValuePay <= 0) {
            bill = "Платеж №" + getCount() + " неуспешный" +
                    "\nна сумму: " + howMach +
                    "\nот: \"" + holderCompany.get(payCompany).name() + "\" " +
                    "\nдля: \"" + holderCompany.get(receiverCompany).name() + "\" " +
                    "\nназначение: покупка товара" +
                    "\nпричина отказа: недостаточно средств";
            setBillMemory(getCount(), bill);
            setCount();
            return bill;
        } else {

            holderCompany.get(payCompany).cutDownMoney(payCompany, maxKey, howMach);

            holderCompany.get(receiverCompany).addMoney(receiverCompany,
                    Collections.max(holderCompany.get(receiverCompany).getSummInvoice().keySet()), howMach);
            bill = "Платеж №" + getCount() + " успешный" +
                    "\nна сумму: " + howMach +
                    "\nот: \"" + holderCompany.get(payCompany).name() + "\" " +
                    "\nдля: \"" + holderCompany.get(receiverCompany).name() + "\" " +
                    "\nназначение: покупка товара";
            setBillMemory(getCount(), bill);
            setCount();
            return bill;
        }
    }

    public void whatBeenBils() {
        for (Map.Entry entry : billMemory.entrySet()) {
            System.out.println(entry.getValue());
            System.out.println("-------------------------------------");
        }
    }

    private String getKeyFromValue(Map hm, double value) {
        for (Object o : hm.keySet()) {
            if (hm.get(o).equals(value)) {
                return o.toString();
            }
        }
        return null;
    }
}
