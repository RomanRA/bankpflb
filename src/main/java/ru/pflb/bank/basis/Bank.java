package ru.pflb.bank.basis;

import lib.NameBankCompanyForHolder;
import ru.pflb.bank.functional.PersonalAccount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static lib.Holder.holderCompany;


/**
 * Имеет название и адрес, содержит в себе список лицевых счетов и список документов.
 * <p>
 * Банк имеет метод создания счёта(invoice), который получает на вход организацию и номер создаваемого счёта,
 * после чего добавляет счёт в свой список счетов и добавляет ссылку на счёт в список счётов организации.
 * <p>
 * Кроме того в банке создаётся платёж, на вход метода подаётся текстом номер счёта списания,
 * текстом номер счёта зачисления, сумма списания и назначение платежа, это всё записывается в соответствующие поля.
 */
public class Bank extends Companies implements CompanyParameters, DocAndPersAccount {
    private PersonalAccount personalAccount = new PersonalAccount();
    private Bills bills = new Bills();
    private String name;
    private String adress;
    private Map<String, List<String>> nameInvoce = new HashMap<>();
    private List<String> persBankAccounts = new ArrayList<>();


    public Bank(String name, String adress) {
        this.name = name;
        this.adress = adress;
    }

    public Map<String, List<String>> getNameInvoce() {
        return nameInvoce;
    }


    public String createInvoice(NameBankCompanyForHolder nameCompany, String invoiceCompany, double money) {

        return personalAccount.createInvoice(nameCompany, invoiceCompany,money);
    }

    public String name() {
        return name;
    }

    public String adress() {
        return adress;
    }

    public List<String> persAccounts() {
        return persBankAccounts;
    }

    public void nameCompanyInvoice(String companyName, List<String> invoice) {
        nameInvoce.put(companyName, invoice);
    }

    public void lookNameCompanyInvoice() {
        for (Map.Entry entry : nameInvoce.entrySet()) {
            System.out.println("Компания: \"" + entry.getKey() +
                    "\" номер счета: " + entry.getValue());
        }
    }
    public String lookNameCompanyInvoice(NameBankCompanyForHolder nameBankCompanyForHolder) {
        String nameCompany = "";
        for (Map.Entry entry : nameInvoce.entrySet()) {
            nameCompany = "Компания: \"" + holderCompany.get(nameBankCompanyForHolder).name() +
                    "\" номер счета: " + entry.getValue();
        }
        return nameCompany;
    }

    public String newBill(NameBankCompanyForHolder payCompany, NameBankCompanyForHolder receiverCompany, double howMach) {
        return bills.bill(payCompany, receiverCompany, howMach);
    }

    public void whatBeenBils() {
        bills.whatBeenBils();
    }
}
