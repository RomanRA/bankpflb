import lib.InitHolder;
import starter.Starter;


public class Main {
    public static void main(String[] args) {
        InitHolder initHolder = new InitHolder();
        initHolder.initialization();

        System.out.println("Let the hunger games begin!!!");

        Starter starter = new Starter();
        starter.getStarted();
    }

}
