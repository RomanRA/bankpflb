package starter;

import static lib.Holder.*;
import static lib.NameBankCompanyForHolder.*;

public class Starter {
    public void getStarted() {
//        Добавляем счета для компаний  (счет 12345.810.9.33333333333 задублирован)
        System.out.println("Добавляем счета для всех компаний");
        holderBank.get(BANK).createInvoice(COMPANY_ROTE_ON_MARS, "12345.810.9.11111111111", 1001);
        holderBank.get(BANK).createInvoice(COMPANY_ROTE_ON_MARS, "12345.810.9.22222222222",1002);
        holderBank.get(BANK).createInvoice(COMPANY_ROTE_ON_MARS, "12345.810.9.33333333333",1003);
        holderBank.get(BANK).createInvoice(MARIN_OCEAN_DIVING, "12345.810.9.33333333333",100);
        holderBank.get(BANK).createInvoice(SHOVEL_AND_FRIENDS, "12345.810.9.44444444444",10);
        System.out.println();
        System.out.println("Добавляем счета для компании, с распечаткой информации");
        System.out.println(holderBank.get(BANK).createInvoice(MARIN_OCEAN_DIVING, "12345.810.9.55555555555", 100));

        System.out.println();
        // Колличество компаий обслуживаемых в банке
        System.out.println("Какое колличество компаий обслуживается в банке");
        System.out.println("В банке обслуживается " + holderBank.get(BANK).getNameInvoce().size() + " компании.");
        holderBank.get(BANK).lookNameCompanyInvoice();
        System.out.println();
        // Добавление денег на счет
        System.out.println("Добавление денег на счет");
        holderCompany.get(COMPANY_ROTE_ON_MARS).addMoney(COMPANY_ROTE_ON_MARS,"12345.810.9.12345611111", 1004);
        System.out.println("===========================================================");
        holderCompany.get(COMPANY_ROTE_ON_MARS).addMoney(COMPANY_ROTE_ON_MARS,"12345.810.9.11111111111", 1000);
        System.out.println("===========================================================");
        holderCompany.get(COMPANY_ROTE_ON_MARS).addMoney(COMPANY_ROTE_ON_MARS,"12345.810.9.33333333333", 700);
        System.out.println("===========================================================");
        holderCompany.get(MARIN_OCEAN_DIVING).addMoney(MARIN_OCEAN_DIVING,"12345.810.9.55555555555", 17000);
        System.out.println("===========================================================");
        holderCompany.get(MARIN_OCEAN_DIVING).addMoney(MARIN_OCEAN_DIVING,"12345.810.9.55555555555", 100);
        System.out.println();
        // Списание денег со счетов
        System.out.println("Списание денег со счетов");
        holderCompany.get(COMPANY_ROTE_ON_MARS).cutDownMoney(COMPANY_ROTE_ON_MARS, "12345.810.9.11111111111", 1005);        System.out.println("===========================================================");
        System.out.println("===========================================================");
        holderCompany.get(MARIN_OCEAN_DIVING).cutDownMoney(MARIN_OCEAN_DIVING,"12345.810.9.55555555555", 100);
        System.out.println();
        //  Проверка баланса по всем счетам
        System.out.println("Проверка баланса по всем счетам");
        System.out.println("COMPANY_ROTE_ON_MARS " + holderCompany.get(COMPANY_ROTE_ON_MARS).getSummInvoice());
        System.out.println("MARIN_OCEAN_DIVING " + holderCompany.get(MARIN_OCEAN_DIVING).getSummInvoice());
        System.out.println("SHOVEL_AND_FRIENDS " + holderCompany.get(SHOVEL_AND_FRIENDS).getSummInvoice());
        System.out.println();
        //  Проверяем общий баланс на счетах компаний
        System.out.println("Проверяем общий баланс на счетах компаний");
        System.out.println("Сумма на всех счетах компании " + holderCompany.get(COMPANY_ROTE_ON_MARS).name()
                + " составляет: " + holderCompany.get(COMPANY_ROTE_ON_MARS).allMoneyOnPersonallAccounComany());
        System.out.println("===========================================================");
        System.out.println("Сумма на всех счетах компании " + holderCompany.get(MARIN_OCEAN_DIVING).name()
                + " составляет: " + holderCompany.get(MARIN_OCEAN_DIVING).allMoneyOnPersonallAccounComany());
        System.out.println("===========================================================");
        System.out.println("Сумма на всех счетах компании " + holderCompany.get(SHOVEL_AND_FRIENDS).name()
                + " составляет: " + holderCompany.get(SHOVEL_AND_FRIENDS).allMoneyOnPersonallAccounComany());
        System.out.println();
        // Создаем пложительную транзакцию между компаниями
        System.out.println("Создаем пложительную транзакцию между компаниями");
        System.out.println(holderBank.get(BANK).newBill(COMPANY_ROTE_ON_MARS, SHOVEL_AND_FRIENDS, 500));
        System.out.println();
        // Создаем отрицательную транзакцию между компаниями
        System.out.println("Создаем отрицательную транзакцию между компаниями");
        System.out.println(holderBank.get(BANK).newBill(MARIN_OCEAN_DIVING, SHOVEL_AND_FRIENDS, 150500));
        System.out.println();
        // Проверяем сохраненные счета
        System.out.println("Проверяем сохраненные счета");
        holderBank.get(BANK).whatBeenBils();
        System.out.println();
        // Повторно проверяем счета компаний
        System.out.println(" Повторно проверяем счета компаний");
        System.out.println("Сумма на всех счетах компании " + holderCompany.get(SHOVEL_AND_FRIENDS).name()
                + " составляет: " + holderCompany.get(SHOVEL_AND_FRIENDS).allMoneyOnPersonallAccounComany());
        System.out.println("===========================================================");
        System.out.println("Сумма на всех счетах компании " + holderCompany.get(COMPANY_ROTE_ON_MARS).name()
                + " составляет: " + holderCompany.get(COMPANY_ROTE_ON_MARS).allMoneyOnPersonallAccounComany());
        System.out.println("===========================================================");
        System.out.println("Сумма на всех счетах компании " + holderCompany.get(MARIN_OCEAN_DIVING).name()
                + " составляет: " + holderCompany.get(MARIN_OCEAN_DIVING).allMoneyOnPersonallAccounComany());
    }
}
