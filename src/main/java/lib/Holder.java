package lib;

import ru.pflb.bank.basis.Bank;
import ru.pflb.bank.basis.Companies;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Holder {
    public static Map<NameBankCompanyForHolder, Bank> holderBank = new HashMap<NameBankCompanyForHolder, Bank>();

    public static Map<NameBankCompanyForHolder, Companies> holderCompany = new HashMap<NameBankCompanyForHolder, Companies>();

}
