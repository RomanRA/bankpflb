package lib;


import ru.pflb.bank.basis.Bank;
import ru.pflb.bank.basis.Companies;

import static lib.Holder.*;
import static lib.NameBankCompanyForHolder.*;

public class InitHolder {
    public void initialization() {
        holderBank.put(BANK, new Bank("ПАО Супер Банк","г Москва, Красная площадь, д.1, офис 1"));
        holderCompany.put(COMPANY_ROTE_ON_MARS, new Companies("Марсианские перевозки",
                "Орбита луны, Главный пояс, станция Орех"));
        holderCompany.put(MARIN_OCEAN_DIVING, new Companies("Морско-Океанский ныряльщик",
                "Очень Тихий Океан, Марианская впадина, 22 трещина, 15 корал"));
        holderCompany.put(SHOVEL_AND_FRIENDS, new Companies("Лопата и ребята",
                "г. Дракулы, Кладищенский округ, ул. Надгробия, 18 часовня"));

    }
}
